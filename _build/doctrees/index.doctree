���i      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�	.. _home:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��home�u�tagname�h	�line�K�parent�hhh�source��9C:\Users\acclayto\Documents\FoundryVTT\app\docs\index.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Foundry Virtual Tabletop�h]�h �Text����Foundry Virtual Tabletop�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �	paragraph���)��}�(h��Welcome to the documentation for **Foundry Virtual Tabletop**, an application for organizing and running tabletop
roleplaying games in a beautiful and intuitive web-based application.�h]�(h.�!Welcome to the documentation for �����}�(h�!Welcome to the documentation for �hh;hhh NhNubh �strong���)��}�(h�**Foundry Virtual Tabletop**�h]�h.�Foundry Virtual Tabletop�����}�(hhhhFubah}�(h]�h]�h]�h]�h]�uhhDhh;ubh.�z, an application for organizing and running tabletop
roleplaying games in a beautiful and intuitive web-based application.�����}�(h�z, an application for organizing and running tabletop
roleplaying games in a beautiful and intuitive web-based application.�hh;hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh �raw���)��}�(h��<iframe width="640" height="360" src="https://www.youtube.com/embed/Iz-2lW3Ac_w" frameborder="0" allow="autoplay;
 encrypted-media" allowfullscreen style="margin-bottom:1em;"></iframe>�h]�h.��<iframe width="640" height="360" src="https://www.youtube.com/embed/Iz-2lW3Ac_w" frameborder="0" allow="autoplay;
 encrypted-media" allowfullscreen style="margin-bottom:1em;"></iframe>�����}�(hhhhaubah}�(h]�h]�h]�h]�h]��format��html��	xml:space��preserve�uhh_h h!hK	hh$hhubh �compound���)��}�(hhh]��sphinx.addnodes��toctree���)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h�index��entries�]�(N�self���N�pages/features���N�	pages/faq���N�pages/hosting���N�pages/tutorial���N�pages/entities���N�
pages/dice���N�	pages/api���N�license���e�includefiles�]�(h�h�h�h�h�h�h�h�e�maxdepth�K�caption��Table of Contents��glob���hidden���includehidden���numbered�K �
titlesonly���
rawcaption�h�uhhyh h!hKhhuubah}�(h]��master-contents�ah]��toctree-wrapper�ah]��master-contents�ah]�h]�uhhshh$hhh h!hNubh#)��}�(hhh]�(h()��}�(h�About Foundry VTT�h]�h.�About Foundry VTT�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hKubh:)��}�(h�6Foundry VTT attempts to innovate in several key areas.�h]�h.�6Foundry VTT attempts to innovate in several key areas.�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK!hh�hhubh �enumerated_list���)��}�(hhh]�(h �	list_item���)��}�(hX  **Built using modern web technologies.** Some of the leading options in the virtual tabletop space suffer from
sluggish application performance. Foundry VTT is built with a highly modern stack of proven web technologies which
provide a great user experience.
�h]�h:)��}�(hX  **Built using modern web technologies.** Some of the leading options in the virtual tabletop space suffer from
sluggish application performance. Foundry VTT is built with a highly modern stack of proven web technologies which
provide a great user experience.�h]�(hE)��}�(h�(**Built using modern web technologies.**�h]�h.�$Built using modern web technologies.�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhhDhh�ubh.�� Some of the leading options in the virtual tabletop space suffer from
sluggish application performance. Foundry VTT is built with a highly modern stack of proven web technologies which
provide a great user experience.�����}�(h�� Some of the leading options in the virtual tabletop space suffer from
sluggish application performance. Foundry VTT is built with a highly modern stack of proven web technologies which
provide a great user experience.�hh�ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK#hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubh�)��}�(hX�  **Intuitive and full-featured user experience.** There are many virtual tabletops out there, some of which are
extremely intuitive and simple to use, others of which are highly complex with an enormous set of features. With
Foundry VTT I attempt to deliver a full-featured product with an incredibly simple and intuitive user experience.
If you or your players have found virtual tabletop software to be confusing in the past - Foundry VTT should feel
like a breath of fresh air. It's simple!
�h]�h:)��}�(hX�  **Intuitive and full-featured user experience.** There are many virtual tabletops out there, some of which are
extremely intuitive and simple to use, others of which are highly complex with an enormous set of features. With
Foundry VTT I attempt to deliver a full-featured product with an incredibly simple and intuitive user experience.
If you or your players have found virtual tabletop software to be confusing in the past - Foundry VTT should feel
like a breath of fresh air. It's simple!�h]�(hE)��}�(h�0**Intuitive and full-featured user experience.**�h]�h.�,Intuitive and full-featured user experience.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhDhh�ubh.X�   There are many virtual tabletops out there, some of which are
extremely intuitive and simple to use, others of which are highly complex with an enormous set of features. With
Foundry VTT I attempt to deliver a full-featured product with an incredibly simple and intuitive user experience.
If you or your players have found virtual tabletop software to be confusing in the past - Foundry VTT should feel
like a breath of fresh air. It’s simple!�����}�(hX�   There are many virtual tabletops out there, some of which are
extremely intuitive and simple to use, others of which are highly complex with an enormous set of features. With
Foundry VTT I attempt to deliver a full-featured product with an incredibly simple and intuitive user experience.
If you or your players have found virtual tabletop software to be confusing in the past - Foundry VTT should feel
like a breath of fresh air. It's simple!�hh�ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK'hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubh�)��}�(hX  **Open to mod developers.** Foundry VTT is built upon a rich front-end JavaScript API which allows for creators
with programming experience to create - well, honestly almost anything! I have designed FoundryVTT to directly
expose the base objects and APIs which control the game to the end user and the product supports easily including
mods. If you have a special mechanic or system that you want to include in your game, the openness of the
Foundry VTT platform allows you to bring that vision to your players.
�h]�h:)��}�(hX   **Open to mod developers.** Foundry VTT is built upon a rich front-end JavaScript API which allows for creators
with programming experience to create - well, honestly almost anything! I have designed FoundryVTT to directly
expose the base objects and APIs which control the game to the end user and the product supports easily including
mods. If you have a special mechanic or system that you want to include in your game, the openness of the
Foundry VTT platform allows you to bring that vision to your players.�h]�(hE)��}�(h�**Open to mod developers.**�h]�h.�Open to mod developers.�����}�(hhhj)  ubah}�(h]�h]�h]�h]�h]�uhhDhj%  ubh.X�   Foundry VTT is built upon a rich front-end JavaScript API which allows for creators
with programming experience to create - well, honestly almost anything! I have designed FoundryVTT to directly
expose the base objects and APIs which control the game to the end user and the product supports easily including
mods. If you have a special mechanic or system that you want to include in your game, the openness of the
Foundry VTT platform allows you to bring that vision to your players.�����}�(hX�   Foundry VTT is built upon a rich front-end JavaScript API which allows for creators
with programming experience to create - well, honestly almost anything! I have designed FoundryVTT to directly
expose the base objects and APIs which control the game to the end user and the product supports easily including
mods. If you have a special mechanic or system that you want to include in your game, the openness of the
Foundry VTT platform allows you to bring that vision to your players.�hj%  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK-hj!  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubh�)��}�(hX�  **System agnostic.** Foundry VTT isn't intended to emphasize a single game system, but rather to provide a rich
framework with an excellent set of core features that allows community members to implement fully-featured game
systems within the environment. I may sponsor a handful of build-in system implementations to help get players
started, but ultimately my vision is that the greatest resources for this product will be its developer community.
�h]�h:)��}�(hX�  **System agnostic.** Foundry VTT isn't intended to emphasize a single game system, but rather to provide a rich
framework with an excellent set of core features that allows community members to implement fully-featured game
systems within the environment. I may sponsor a handful of build-in system implementations to help get players
started, but ultimately my vision is that the greatest resources for this product will be its developer community.�h]�(hE)��}�(h�**System agnostic.**�h]�h.�System agnostic.�����}�(hhhjP  ubah}�(h]�h]�h]�h]�h]�uhhDhjL  ubh.X�   Foundry VTT isn’t intended to emphasize a single game system, but rather to provide a rich
framework with an excellent set of core features that allows community members to implement fully-featured game
systems within the environment. I may sponsor a handful of build-in system implementations to help get players
started, but ultimately my vision is that the greatest resources for this product will be its developer community.�����}�(hX�   Foundry VTT isn't intended to emphasize a single game system, but rather to provide a rich
framework with an excellent set of core features that allows community members to implement fully-featured game
systems within the environment. I may sponsor a handful of build-in system implementations to help get players
started, but ultimately my vision is that the greatest resources for this product will be its developer community.�hjL  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK3hjH  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubh�)��}�(hXM  **No gating or premium features.** With Foundry VTT you won't have to pay for features which other options treat
as "premium features". Features like dynamic lighting, compendiums and character vaults, interactive character
sheets, audio controls, triggers, linked map transitions, and more all will be included in the base product.
�h]�h:)��}�(hXL  **No gating or premium features.** With Foundry VTT you won't have to pay for features which other options treat
as "premium features". Features like dynamic lighting, compendiums and character vaults, interactive character
sheets, audio controls, triggers, linked map transitions, and more all will be included in the base product.�h]�(hE)��}�(h�"**No gating or premium features.**�h]�h.�No gating or premium features.�����}�(hhhjw  ubah}�(h]�h]�h]�h]�h]�uhhDhjs  ubh.X0   With Foundry VTT you won’t have to pay for features which other options treat
as “premium features”. Features like dynamic lighting, compendiums and character vaults, interactive character
sheets, audio controls, triggers, linked map transitions, and more all will be included in the base product.�����}�(hX*   With Foundry VTT you won't have to pay for features which other options treat
as "premium features". Features like dynamic lighting, compendiums and character vaults, interactive character
sheets, audio controls, triggers, linked map transitions, and more all will be included in the base product.�hjs  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK8hjo  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubh�)��}�(hXx  **No reliance on an external service or platform.** As game masters we invest hundreds (if not thousands) of
hours painstakingly crafting rich worlds. That creativity and artistic expression should not be tied to one single
platform or service. Since Foundry VTT is self-hosted, you will always be able to rely upon the software to run
your game sessions. There is never any risk of a required service getting shut off or going away. Game data is
stored in an embedded database which means that even if you end up moving away from Foundry VTT in the future, you
will still have all your data that you can migrate to a new platform.
�h]�h:)��}�(hXw  **No reliance on an external service or platform.** As game masters we invest hundreds (if not thousands) of
hours painstakingly crafting rich worlds. That creativity and artistic expression should not be tied to one single
platform or service. Since Foundry VTT is self-hosted, you will always be able to rely upon the software to run
your game sessions. There is never any risk of a required service getting shut off or going away. Game data is
stored in an embedded database which means that even if you end up moving away from Foundry VTT in the future, you
will still have all your data that you can migrate to a new platform.�h]�(hE)��}�(h�3**No reliance on an external service or platform.**�h]�h.�/No reliance on an external service or platform.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubh.XD   As game masters we invest hundreds (if not thousands) of
hours painstakingly crafting rich worlds. That creativity and artistic expression should not be tied to one single
platform or service. Since Foundry VTT is self-hosted, you will always be able to rely upon the software to run
your game sessions. There is never any risk of a required service getting shut off or going away. Game data is
stored in an embedded database which means that even if you end up moving away from Foundry VTT in the future, you
will still have all your data that you can migrate to a new platform.�����}�(hXD   As game masters we invest hundreds (if not thousands) of
hours painstakingly crafting rich worlds. That creativity and artistic expression should not be tied to one single
platform or service. Since Foundry VTT is self-hosted, you will always be able to rely upon the software to run
your game sessions. There is never any risk of a required service getting shut off or going away. Game data is
stored in an embedded database which means that even if you end up moving away from Foundry VTT in the future, you
will still have all your data that you can migrate to a new platform.�hj�  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK<hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubeh}�(h]�h]�h]�h]�h]��enumtype��arabic��prefix�h�suffix��.�uhh�hh�hhh h!hK#ubeh}�(h]��about-foundry-vtt�ah]�h]��about foundry vtt�ah]�h]�uhh"hh$hhh h!hKubh �
transition���)��}�(h�-----�h]�h}�(h]�h]�h]�h]�h]�uhj�  h h!hKChh$hhubh#)��}�(hhh]�(h()��}�(h�Hosting Modes�h]�h.�Hosting Modes�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj�  hhh h!hKGubh:)��}�(h�EThere are two primary means of hosting a game session in Foundry VTT.�h]�h.�EThere are two primary means of hosting a game session in Foundry VTT.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKIhj�  hhubh�)��}�(hhh]�(h�)��}�(h��**Peer-to-peer using a native application.** In this mode, the session host runs an app on their own computer which
hosts the game session and players connect directly to the hosted game.
�h]�h:)��}�(h��**Peer-to-peer using a native application.** In this mode, the session host runs an app on their own computer which
hosts the game session and players connect directly to the hosted game.�h]�(hE)��}�(h�,**Peer-to-peer using a native application.**�h]�h.�(Peer-to-peer using a native application.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhDhj  ubh.�� In this mode, the session host runs an app on their own computer which
hosts the game session and players connect directly to the hosted game.�����}�(h�� In this mode, the session host runs an app on their own computer which
hosts the game session and players connect directly to the hosted game.�hj  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKKhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhh h!hNubh�)��}�(h��**Dedicated standalone server.** Foundry VTT also supports a "headless" installation on a Linux or Windows server
which allows the game to be persistent where both players and GMs can connect to the world.

�h]�h:)��}�(h��**Dedicated standalone server.** Foundry VTT also supports a "headless" installation on a Linux or Windows server
which allows the game to be persistent where both players and GMs can connect to the world.�h]�(hE)��}�(h� **Dedicated standalone server.**�h]�h.�Dedicated standalone server.�����}�(hhhj-  ubah}�(h]�h]�h]�h]�h]�uhhDhj)  ubh.�� Foundry VTT also supports a “headless” installation on a Linux or Windows server
which allows the game to be persistent where both players and GMs can connect to the world.�����}�(h�� Foundry VTT also supports a "headless" installation on a Linux or Windows server
which allows the game to be persistent where both players and GMs can connect to the world.�hj)  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKNhj%  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhh h!hNubeh}�(h]�h]�h]�h]�h]�j�  j�  j�  hj�  j�  uhh�hj�  hhh h!hKKubeh}�(h]��hosting-modes�ah]�h]��hosting modes�ah]�h]�uhh"hh$hhh h!hKGubj�  )��}�(h�-----�h]�h}�(h]�h]�h]�h]�h]�uhj�  h h!hKRhh$hhubh#)��}�(hhh]�(h()��}�(h�Try the Demo�h]�h.�Try the Demo�����}�(hji  hjg  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hjd  hhh h!hKVubh:)��}�(hX
  Please feel free to try a working demo of my progress thus far. As I develop and refine new features in my local test
environment I am pushing them (often daily) to the development/demo server. Please feel free to check back frequently
to keep an eye on my progress!�h]�h.X
  Please feel free to try a working demo of my progress thus far. As I develop and refine new features in my local test
environment I am pushing them (often daily) to the development/demo server. Please feel free to check back frequently
to keep an eye on my progress!�����}�(hjw  hju  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKXhjd  hhubh:)��}�(h�VAs you are visiting the demo, please be aware of several key factors and ground rules.�h]�h.�VAs you are visiting the demo, please be aware of several key factors and ground rules.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK\hjd  hhubh�)��}�(hhh]�(h�)��}�(h��This demo is "pre-Alpha". I am not yet near feature complete. There will be bugs. There will be things that do not
work at all. There will be things that are obviously incomplete.
�h]�h:)��}�(h��This demo is "pre-Alpha". I am not yet near feature complete. There will be bugs. There will be things that do not
work at all. There will be things that are obviously incomplete.�h]�h.��This demo is “pre-Alpha”. I am not yet near feature complete. There will be bugs. There will be things that do not
work at all. There will be things that are obviously incomplete.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hK^hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhh h!hNubh�)��}�(h��I am hosting this demo server for a limited number of sample player accounts. Please don't monopolize the limited
slots by leaving your browser connected for hours.
�h]�h:)��}�(h��I am hosting this demo server for a limited number of sample player accounts. Please don't monopolize the limited
slots by leaving your browser connected for hours.�h]�h.��I am hosting this demo server for a limited number of sample player accounts. Please don’t monopolize the limited
slots by leaving your browser connected for hours.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKahj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhh h!hNubh�)��}�(h�]I will reset the demo regularly back to a "safe state" - do not expect any data to be saved.
�h]�h:)��}�(h�\I will reset the demo regularly back to a "safe state" - do not expect any data to be saved.�h]�h.�`I will reset the demo regularly back to a “safe state” - do not expect any data to be saved.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKdhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhh h!hNubh�)��}�(h�Don't be a jerk.
�h]�h:)��}�(h�Don't be a jerk.�h]�h.�Don’t be a jerk.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKfhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhh h!hNubeh}�(h]�h]�h]�h]�h]�j�  j�  j�  hj�  j�  uhh�hjd  hhh h!hK^ubh �note���)��}�(h�jThe current Foundry VTT demo password is ``foundry``. This password may rotate if needed to prevent abuse.�h]�h:)��}�(hj�  h]�(h.�)The current Foundry VTT demo password is �����}�(h�)The current Foundry VTT demo password is �hj   ubh �literal���)��}�(h�``foundry``�h]�h.�foundry�����}�(hhhj
  ubah}�(h]�h]�h]�h]�h]�uhj  hj   ubh.�6. This password may rotate if needed to prevent abuse.�����}�(h�6. This password may rotate if needed to prevent abuse.�hj   ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hjd  hhh h!hNubh:)��}�(h�PThose disclaimers aside, you can find the demo here: http://foundryvtt.com:30000�h]�(h.�5Those disclaimers aside, you can find the demo here: �����}�(h�5Those disclaimers aside, you can find the demo here: �hj)  hhh NhNubh �	reference���)��}�(h�http://foundryvtt.com:30000�h]�h.�http://foundryvtt.com:30000�����}�(hhhj4  ubah}�(h]�h]�h]�h]�h]��refuri�j6  uhj2  hj)  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKjhjd  hhubeh}�(h]��try-the-demo�ah]�h]��try the demo�ah]�h]�uhh"hh$hhh h!hKVubj�  )��}�(h�-----�h]�h}�(h]�h]�h]�h]�h]�uhj�  h h!hKmhh$hhubh#)��}�(hhh]�(h()��}�(h�Join our Discord�h]�h.�Join our Discord�����}�(hj`  hj^  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj[  hhh h!hKpubj3  )��}�(hhh]�h �image���)��}�(h��.. image:: http://ashenfoundry.s3.amazonaws.com/media/user_1/screen/discord_banner-2017-07-09.png
    :target: https://discordapp.com/invite/DDBZUDf
�h]�h}�(h]�h]�h]�h]�h]��uri��Vhttp://ashenfoundry.s3.amazonaws.com/media/user_1/screen/discord_banner-2017-07-09.png��
candidates�}��?�j|  suhjo  hjl  h h!hNubah}�(h]�h]�h]�h]�h]��refuri��%https://discordapp.com/invite/DDBZUDf�uhj2  hj[  hhh h!hNubh:)��}�(hX�  If you would like to get involved in Foundry Virtual Tabletop development and participate in conversation with an
active group of fellow RPG enthusiasts, please join us in `Discord <https://discordapp.com/invite/DDBZUDf>`_.
The Discord server is the best place to get daily updates on product development, learn about testing opportunities,
and provide feedback on how Foundry VTT can achieve its greatest potential. Hope to see you there!�h]�(h.��If you would like to get involved in Foundry Virtual Tabletop development and participate in conversation with an
active group of fellow RPG enthusiasts, please join us in �����}�(h��If you would like to get involved in Foundry Virtual Tabletop development and participate in conversation with an
active group of fellow RPG enthusiasts, please join us in �hj�  hhh NhNubj3  )��}�(h�2`Discord <https://discordapp.com/invite/DDBZUDf>`_�h]�h.�Discord�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��Discord��refuri��%https://discordapp.com/invite/DDBZUDf�uhj2  hj�  ubh
)��}�(h�( <https://discordapp.com/invite/DDBZUDf>�h]�h}�(h]��discord�ah]�h]��discord�ah]�h]��refuri�j�  uhh	�
referenced�Khj�  ubh.��.
The Discord server is the best place to get daily updates on product development, learn about testing opportunities,
and provide feedback on how Foundry VTT can achieve its greatest potential. Hope to see you there!�����}�(h��.
The Discord server is the best place to get daily updates on product development, learn about testing opportunities,
and provide feedback on how Foundry VTT can achieve its greatest potential. Hope to see you there!�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKuhj[  hhubeh}�(h]��join-our-discord�ah]�h]��join our discord�ah]�h]�uhh"hh$hhh h!hKpubeh}�(h]�(�foundry-virtual-tabletop�heh]�h]�(�foundry virtual tabletop��home�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�j�  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(j�  hj�  j�  h�h�j�  j�  jW  jT  jN  jK  j�  j�  j�  j�  u�	nametypes�}�(j�  �j�  Nh��j�  NjW  NjN  Nj�  Nj�  �uh}�(hh$j�  h$h�huj�  h�jT  j�  jK  jd  j�  j[  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h:)��}�(hhh]�h.�*Hyperlink target "home" is not referenced.�����}�(hhhjS  ubah}�(h]�h]�h]�h]�h]�uhh9hjP  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�KuhjN  uba�transformer�N�
decoration�Nhhub.