Frequently Asked Questions
**************************

**How far along is this product in development? What is the timeline for testing phases? When will this be released?**

I am an independent developer working on this as a hobby project. I am not working on this full time, however, I am
making fast progress and the software is now in closed Alpha testing (since November) for Patreon supporters and friends.

I am actively collecting feedback and working to continue improving the software for Beta testing. I expect Closed beta 
testing phases to begin in mid-February, 2018.

I expect Beta testing to occur for 5-6 months which would potentially allow for an initial release version of the software 
around August, 2019. This schedule is highly subject to change, however, so please be sure to check back for future 
updates.

Opportunities to participate in Alpha or Beta testing phases is available by supporting the project on Patreon at
https://patreon.com/foundryvtt.

-------

**Why are you creating this software?**

My primary investment in this project is not as a business venture but rather as a fan of the tabletop gaming genre
and a dungeon master who loves creating rich experiences for players. I believe the tabletop RPG community deserves
better from its software and I want to contribute towards that by creating technology that I would love to use and 
I think there's a lot of opportunity for my work to benefit the gaming community.

-------

**What will this cost? What is your business model?**

While my foremost objective is not business development, I do want to approach the project with a level of seriousness 
that could allow for it to grow and support whatever community chooses to form around it.

Foundry VTT will be sold as individual licenses sold for a moderate fee. I plan to price each individual license 
somewhere in the $30 to $50 range depending on the extent of features that FVTT offers and the quality of the product
at the time of release. 

Each individual license will allow a game master to host games for unlimited players (who do not need to purchase). 
I have no plans at this time to offer any sort of subscription service and there will not be any premium features or 
gated purchase tiers.

Additionally, I have a Patreon page where community members who wish to provide additional support and have more 
direct developer access to participate in development vision and prioritization discussions. Patreon pledges are 
absolutely not required and do not modify game features, but during the pre-release phases of the software Patreon 
supporters do recieve early access to the software. 

For Patreon supporters who have contributed towards the project during the testing phases, I plan to offer a discount
on the final purchase price based on the amount or length of support during the testing process.

-------

**How long will beta testing last? What does Beta testing mean for Alpha supporters?**

The closed Alpha testing will continue through release, along-side Beta testing. Both test phases will continue 
concurrently. I want to provide some perks/rewards for those of you who continue supporting at the Alpha tiers. My current 
plan is to have two build versions, one for beta testers which is (hopefully) more stable but receives less frequent 
updates and another for Alpha testers which has more frequent updates allowing them to get their hands on new features 
first. Alpha testers will, of course, have the option of using the stable Beta build if they prefer that to the latest version.

-------

**How frequently do the software updates occur?**

Since the beginning of Alpha testing in November, 2018 I have been maintaining a pace of releasing a new update version 
approximately every 7-10 days. I intend to maintain this pace (with some exceptions) until the full product release.

-------

**Will there be an open beta that is available to anyone, free of charge?**

Not at this time. My plans are to only do a closed beta for Patreon supporters.

-------

**What will be the Patreon support fee to participate in the closed Beta test?**

Just before the beta test begins I will be locking the $3/month supporter tier as a way to say "thank you" to everyone who has 
been supporting this project since an early date. Those supporters will lock-in that support level throughout the closed beta 
cycle. For additional supporters I will be opening a $5/month tier to opt-in to the closed beta phases.

-------

**Will worlds or content created using the Beta build be valid for use once the release is made available?**

Yes. All your content or creations will carry forward. I do not anticipate any significant issues with changes that break 
backwards compatibility. Please feel empowered to begin creating worlds that you plan to use for a long time!


Thanks and Attributions
-----------------------

* **Audio** Thanks to Mike Koenig, and other Contributors: https://soundbible.com

* **Audio** Thanks to Dymewiz and other Contributors: https://freesound.org

* **Spell Icons** Thanks to J. W. Bjerk (eleazzar) for "Painterly Spell Icons" series: https://opengameart.org

* **SVG Icons** Thanks to Contributors, https://game-icons.net

* **Potion Artwork** Thanks to Melle, https://opengameart.org/content/fantasy-potion-set

