.. _diceAPI:

The Dice Rolling API
********************

For mod developers, you may want to customize in-depth the behavior of dice rolling. You can interact directly with
the Roll API by creating instances of the :class:`Roll` class.

..  autoclass:: Roll
    :members:
