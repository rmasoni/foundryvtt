.. _home:

Foundry Virtual Tabletop
************************

Welcome to the documentation for **Foundry Virtual Tabletop**, an application for organizing and running tabletop
roleplaying games in a beautiful and intuitive web-based application.

.. raw:: html

    <iframe width="640" height="360" src="https://www.youtube.com/embed/Iz-2lW3Ac_w" frameborder="0" allow="autoplay;
     encrypted-media" allowfullscreen style="margin-bottom:1em;"></iframe>

..  toctree::
    :caption: Table of Contents
    :name: master-contents
    :maxdepth: 1

    self
    pages/features
    pages/faq
    pages/hosting
    pages/tutorial
    pages/entities
    pages/dice
    pages/api
    license


About Foundry VTT
=================

Foundry VTT attempts to innovate in several key areas.

1.  **Built using modern web technologies.** Some of the leading options in the virtual tabletop space suffer from
    sluggish application performance. Foundry VTT is built with a highly modern stack of proven web technologies which
    provide a great user experience.

2.  **Intuitive and full-featured user experience.** There are many virtual tabletops out there, some of which are
    extremely intuitive and simple to use, others of which are highly complex with an enormous set of features. With
    Foundry VTT I attempt to deliver a full-featured product with an incredibly simple and intuitive user experience.
    If you or your players have found virtual tabletop software to be confusing in the past - Foundry VTT should feel
    like a breath of fresh air. It's simple!

3.  **Open to mod developers.** Foundry VTT is built upon a rich front-end JavaScript API which allows for creators
    with programming experience to create - well, honestly almost anything! I have designed FoundryVTT to directly
    expose the base objects and APIs which control the game to the end user and the product supports easily including
    mods. If you have a special mechanic or system that you want to include in your game, the openness of the
    Foundry VTT platform allows you to bring that vision to your players.

4.  **System agnostic.** Foundry VTT isn't intended to emphasize a single game system, but rather to provide a rich
    framework with an excellent set of core features that allows community members to implement fully-featured game
    systems within the environment. I may sponsor a handful of build-in system implementations to help get players
    started, but ultimately my vision is that the greatest resources for this product will be its developer community.

5.  **No gating or premium features.** With Foundry VTT you won't have to pay for features which other options treat
    as "premium features". Features like dynamic lighting, compendiums and character vaults, interactive character
    sheets, audio controls, triggers, linked map transitions, and more all will be included in the base product.

6.  **No reliance on an external service or platform.** As game masters we invest hundreds (if not thousands) of
    hours painstakingly crafting rich worlds. That creativity and artistic expression should not be tied to one single
    platform or service. Since Foundry VTT is self-hosted, you will always be able to rely upon the software to run
    your game sessions. There is never any risk of a required service getting shut off or going away. Game data is
    stored in an embedded database which means that even if you end up moving away from Foundry VTT in the future, you
    will still have all your data that you can migrate to a new platform.

-----


Hosting Modes
=============

There are two primary means of hosting a game session in Foundry VTT.

1.  **Peer-to-peer using a native application.** In this mode, the session host runs an app on their own computer which
    hosts the game session and players connect directly to the hosted game.

2.  **Dedicated standalone server.** Foundry VTT also supports a "headless" installation on a Linux or Windows server
    which allows the game to be persistent where both players and GMs can connect to the world.


-----


Try the Demo
============

Please feel free to try a working demo of my progress thus far. As I develop and refine new features in my local test
environment I am pushing them (often daily) to the development/demo server. Please feel free to check back frequently
to keep an eye on my progress!

As you are visiting the demo, please be aware of several key factors and ground rules.

1.  This demo is "pre-Alpha". I am not yet near feature complete. There will be bugs. There will be things that do not
    work at all. There will be things that are obviously incomplete.

2.  I am hosting this demo server for a limited number of sample player accounts. Please don't monopolize the limited
    slots by leaving your browser connected for hours.

3.  I will reset the demo regularly back to a "safe state" - do not expect any data to be saved.

4.  Don't be a jerk.

.. note:: The current Foundry VTT demo password is ``foundry``. This password may rotate if needed to prevent abuse.

Those disclaimers aside, you can find the demo here: http://foundryvtt.com:30000


-----

Join our Discord
================

.. image:: http://ashenfoundry.s3.amazonaws.com/media/user_1/screen/discord_banner-2017-07-09.png
    :target: https://discordapp.com/invite/DDBZUDf

If you would like to get involved in Foundry Virtual Tabletop development and participate in conversation with an
active group of fellow RPG enthusiasts, please join us in `Discord <https://discordapp.com/invite/DDBZUDf>`_.
The Discord server is the best place to get daily updates on product development, learn about testing opportunities,
and provide feedback on how Foundry VTT can achieve its greatest potential. Hope to see you there!
